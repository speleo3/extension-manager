This readme file should be replaced by your extensions information. But for now
it will contain further information about how to make a package.

This README.md file will end up as the packages "long description" in the meta data for the package.

# Copy and Delete

Copy the template directory to a new location, make sure you bring all files with you.

Delete the parts you don't need, such as the inx, py or module files if you already have
these parts and don't need them (and have already read them so understand how to
modify your own code)

# Edit Everything

Every single file in the template will need editing. (including THIS file!)

 - setup.py will need to contain every bit of metadata requested, every module, every script, every inx file.
 - my-extension.inx will need to make sure it's pointing at your scripts which will now be in bin
 - my-extension.py will need to make sure it's including inkpath if needed
 - icon.svg will need a snazzy new cool icon for your extension. Ask if you need help. OpenClipart can provide useful SVG examples. Icon should be 64x64 square and as simple as possible, color is allowed, it will often be shown on a white background, but also a blue one when list items are selected.
 - README.md will need to be re-written with your own things to communicate to the extension users.

# Commit it to GIT

If you are not already using git for your project, then this is a good time to learn. Create a new git repository and make sure you
commit all the files and push to a remote server. You can use this address in the setup.py file as the url location where
development happens.

    http://rogerdudler.github.io/git-guide/

# System Requirements

There may be some dependancies that you will need to install or have available before you can begin this step. Some of these can be installed by your system's package manager, i.e. apt-get in Debian/Ubuntu and others can be installed using pip (once you have pip instaled)

 a. python, install python2 and python3 and make sure you test your extension with both
 b. pip, this is a python package, it can be installed system wide, but also installed locally if you you need to. You will have it already if you have the inkscape extensions manager.
 c. twine, this is a package that lets you interact with PyPI (uploading packages)
 d. Inkscape Extensions Manager, installed locally or system wide.

# Building your Package

Find the location of your inkman installation. This is because you will need it during the building process. If I installed
the inkscape extension manager into ~/.config/inkscape/extensions I would add that to the PYTHONPATHs below

Once you have an extension, you can test to see if it builds with:

    PYTHONPATH=~/.config/inkscape/extensions python setup.py sdist

This builds a source package, and it will list out all the files that are now in your package. If some of your files are
missing, then you have to adjust the MANIFEST.in file to include any extra files. And be sure to tell setup.py where
these should be installed. This includes data files used by your extension.

# Putting it on Wheels

We can now build the package into a wheel. this is a python binary package which is then uploaded to pypi.

    PYTHONPATH=~/.config/inkscape/extensions python setup.py bdist_wheel

The new wheel file will be in the folder `dist`

# Pushing it to PYPI

You will need an account on pypi.org before you can upload your package, once you have one, you can upload your package using twine (note, username and password can be saved into ~/.pypirc for quick use by these tools):

    twine upload dist/my-extension-0.4-py2-none-any.whl

Uploading the package to test-pypi (this is a seperate server where you can test your depoyment before uploading to the live system)

    twine upload --repository-url https://test.pypi.org/legacy/ dist/my-extension-py2-none-any.whl

You may only upload any version of your package ONCE, then you must bump (increase) the version number and rebuild the wheel (above) to make further updates to the live wheel. More settings to control the distribution exist on the pypi website.

    More information: https://packaging.python.org/tutorials/packaging-projects/

# Have a cup of tea

This part is critical. You will need to make a full pot of Assam tea. And get some bone china tea cups out. We've
heard from some developers that coffee has worked, but we're not currently supporting coffee so YMMV. All other
tea settings are yours to make.

But you're done. Well done!

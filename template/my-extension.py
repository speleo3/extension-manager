#!/usr/bin/env python

# Load up the required paths to access installed modules, if you don't
# Ask for inkpath first, then you'll not be able to access either your
# own modules (mymodule in this template) or inbuilt modules like inkex
#
# But it isn't required if your extension is self contained or is accessing
# other files in the bin directory. Other files in bin are on the python-path
# so they'll be available without including inkpath.
#
import inkpath

# Test that we can load a module
import mymodule

# === REST as usual for an extension === #

import sys
sys.stderr.write("This is a fake extension.\n")


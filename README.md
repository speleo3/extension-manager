# Inkscape Extensions Manager

This Gtk3 application is actually an Inkscape Extension which manages
other Inkscape extensions. It's intended to be installed alongside
Inkscape and also to be shipped with Inkscape.

## Installation

It shouldn't be required to install it. But, you can install it via pip
into Inkscape's extensions directory if you are testing or if you find
yourself messed up for some reason and need to install just this extension.

### Linux Installation

    pip install --isolated --target ~/.config/inkscape/extensions inkscape-extensions-manager

This command will install the extension into the target directory without much
of python's usual directory structure.

## Development

It's possible to run the extensions manager from the development directory, this is important
for development. The important thing to remeber is that finding what packages are installed
as well as what directory to install packages to is defined by the PYTHONPATH defined when
you run the package.

    PYTHONPATH=.:{TARGETDIR1}:{TARGETDIR2} ./manage-extensions.py

The first PYTHONPATH item '.' means that all the extension libraries come from this directory
next each directory should point to an Inkscape extensions location. For example on a Linux
computer this would look like this:

    PYTHONPATH=.:~/.config/inkscape/extensions:/usr/share/inkscape/extensions ./manage-extensions.py



#
# Copyright (C) 2019 Martin Owens
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
"""
Searching for external packages and getting meta data about them.
"""

import os
import json
import logging

import pip
try:
    from pip._internal.models.index import PyPI
except ImportError:
    from pip.models import PyPI
from pip._vendor.distlib.locators import Locator
from pip._vendor.distlib.compat import urljoin, quote
from pip._vendor.distlib.util import ensure_slash

from .package import Package, PackageLists
from .cache import CacheManager

SERVERS = [
    PyPI.pypi_url, # Live PyPi
    "https://test.pypi.org/pypi/", # Test PyPi
]
SERVER_NAME = ['live', 'test']

class RemoteArchive(object):
    """
    Control calling out to pip to search for extensions in the package index.
    """
    def __init__(self, **kwargs):
        self.test_server = kwargs.pop('test_server', False)
        self.index = kwargs.pop('index', SERVERS[self.test_server])
        self.cache = CacheManager(suffix=SERVER_NAME[self.test_server])
        self.cache_dir = os.path.join(self.cache.path, 'archive')

    def search(self, query, filtered=True):
        """
        Search for extension packages

        If filtered is True, only shows extension packages
        """
        class SearchOptions(object):
            """These are settings used by the searcher"""
            retries = 3
            trusted_hosts = []
            cert = None
            client_cert = None
            timeout = 1400
            proxy = None
            no_input = True
            cache_dir = self.cache_dir
            index = self.index

        searcher = pip.commands.SearchCommand()
        for item in searcher.search(query, SearchOptions()):
            package = RemotePackage(self, item['name'], item['version'])
            if not filtered or package.is_extension():
                yield package

class InstallableMixin(object):
    def install_to(self, path):
        """Install this remote package to the local directory"""
        installer = pip.commands.InstallCommand()
        args = ['--no-deps', '--isolated', '--prefix={}'.format(path)]
        if self.index:
            args.append('--index-url={}'.format(self.index))

        ret = installer.main(args + [self.pkg_name])
        self.installed = PackageLists().get_package(self.name)
        return ret


class FilePackage(InstallableMixin):
    def __init__(self, filename):
        if not os.path.isfile(filename):
            raise IOError("Can not find file: {}".format(filename))
        self.index = None
        self.pkg_name = filename
        self.name = os.path.basename(filename)


class RemotePackage(InstallableMixin, Package):
    """
    Gets information about the remote package using requests.
    """
    def __init__(self, archive, name, version):
        self.index = archive.index
        self._metadata = None
        self.pkg_name = name
        self.pkg_version = version
        self.cache_name = "{0.pkg_name}-{0.pkg_version}".format(self)
        self.cache = archive.cache
        self.locator = MetadataLocator(self.index)
        self.installed = PackageLists().get_package(self.name)

    def get_metadata(self):
        """Meta data is a directly added item"""
        if self._metadata is None:
            self._metadata = self.cache.cache_or(
                self.cache_name,
                self.locator.get_metadata, self.pkg_name,
            )['info']
        return self._metadata


class MetadataLocator(Locator):
    """Find the metadata for a package"""
    def __init__(self, index_url):
        super(MetadataLocator, self).__init__()
        self.base_url = ensure_slash(index_url)

    def get_distribution_names(self):
        raise NotImplementedError("Does not return distriction names")

    def get_metadata(self, name):
        """Returns the raw metadata parsed json"""
        url = urljoin(self.base_url, '%s/json' % quote(name))
        try:
            resp = self.opener.open(url)
            data = resp.read().decode() # for now
            return json.loads(data)
        except Exception as err: # pylint: disable=broad-except
            logging.error("Failed to get JSON: %s < %s", str(err), url)
        return None

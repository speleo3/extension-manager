#
# Copyright (C) 2019 Martin Owens
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
"""
Support for constructing and understanding installed extensions
as python packages.

Start by using 'PackageLists' and iterate over to get 'PackageList' objects
iterate over those to get 'Package' objects and use those to know more
information about the installed packages.
"""

import os
import sys
import json
import logging

from .utils import DATA_DIR, ICON_SEP, parse_metadata, clean_author

# The ORDER of these places is important, when installing packages it will
# choose the FIRST writable directory in the lsit, so make sure that the
# more preferable locations appear at the top.
EXTENSION_PLACES = []

for path in sys.path:
    if 'extensions' in path:
        if '/bin' in path:
            path = path.replace('/bin', '')
        EXTENSION_PLACES.append(path)

DEFAULT_ICON = os.path.join(DATA_DIR, 'default_icon.svg')
MODULE_ICON = os.path.join(DATA_DIR, 'module_icon.svg')

EXTENSION_FILTERS = {
    'classifiers': [
        'Environment :: Plugins',
        'Topic :: Multimedia :: Graphics :: Editors :: Vector-Based',
    ]
}


class PackageLists(object):
    """
    A list of package lists where extensions are found.
    """
    def __init__(self, filtered=True):
        self.filtered = filtered

    def __iter__(self):
        for loc in EXTENSION_PLACES:
            loc = os.path.abspath(os.path.expanduser(loc))
            if os.path.isdir(loc):
                yield PackageList(loc, filtered=self.filtered)

    def get_package(self, name, version=None):
        """Test every packages list for the given package name, test version"""
        for packages in self:
            found = packages.get_package(name, version=version)
            if found:
                return found
        return None

    def install_location(self):
        """Get loction where we can install a package"""
        for package_list in self:
            if package_list.is_writable():
                return package_list
        return None


class PackageList(object):
    """
    A list of packages based on a target directory
    """
    def __init__(self, path, filtered=True):
        self.path = os.path.abspath(path)
        self.filtered = filtered

    def __str__(self):
        if '/.' in self.path:
            return "User extensions '.{}'".format(self.path.split('/.', 1)[-1])
        return "System extension '{}'".format(self.path)

    def __repr__(self):
        return "<PackageList '{}'>".format(str(self))

    def __iter__(self):
        return self.iter()

    def iter(self, filtered=None):
        if filtered is None:
            filtered = self.filtered
        for node in self.get_package_paths():
            if node.endswith('.dist-info') or node.endswith('.egg-info'):
                package = InstalledPackage(os.path.join(self.path, node), self.path)
                if not filtered or package.is_extension():
                    yield package

    def get_package_paths(self):
        pyver = "python" + sys.version[:3]
        for varient in [
                os.path.join(self.path, "lib", pyver, "site-packages"),
            ]:
            if os.path.isdir(varient):
                for subpath in os.listdir(varient):
                    yield os.path.join(varient, subpath)

    def is_writable(self):
        """Can the folder be written to (for installation)"""
        return os.access(self.path, os.W_OK)

    def get_package(self, name, version=None):
        """Test every package in this list if it matches the name and version"""
        for package in self.iter(filtered=False):
            found = package.is_package(name, version=version)
            if found:
                return package
        return None

    def untracked_files(self):
        """Returns a list of files which are not tracked by any package"""
        raise NotImplementedError("Need to write this function")
        list_of_files = os.walk(self.path)
        for package in self:
            pass


class Package(object):
    """
    A single package, either wheel dist or egg.
    """
    name = property(lambda self: self.get_metadata()['name'] or '')
    summary = property(lambda self: self.get_metadata()['summary'] or '')
    version = property(lambda self: self.get_metadata()['version'] or '')
    author = property(lambda self: self.get_metadata()['author'] or '')

    def __str__(self):
        return self.name

    def is_package(self, name, version=None):
        """Test if this package matches the given name and version"""
        mdat = self.get_metadata()
        if mdat['name'] == name:
            if version is None or self.is_version(version):
                return True
        return False

    def is_version(self, version):
        """Test if this package version is up to date"""
        # XXX We could do with using the real version checker from python packages
        if self.version >= version:
            return True
        return False

    def is_extension(self):
        """Returns True if this is an inkscape extension"""
        mdat = self.get_metadata()
        for clf in EXTENSION_FILTERS['classifiers']:
            if clf not in mdat['classifiers']:
                return False
        return True

    def get_metadata(self):
        """Returns the metadata from an array of known types"""
        raise NotImplementedError("get_metadata is needed in child class.")

    def get_icon(self):
        """Returns the icon associated with this package"""
        desc = self.get_description()
        if desc and ICON_SEP in desc:
            return desc.split(ICON_SEP)[-1].strip()
        icon = DEFAULT_ICON if self.is_extension() else MODULE_ICON
        with open(icon, 'r') as fhl:
            return fhl.read()

    def get_description(self):
        """Returns the description from the file or the metadata"""
        return self.get_metadata().get('description', None)

class InstalledPackage(Package):
    """
    A package in a specific location.
    """
    def __init__(self, info_path, container=''):
        if not os.path.isdir(info_path):
            raise IOError("Can't find package directory: {}".format(info_path))
        self._metadata = None
        self.path = info_path.rstrip('/')
        self.base = os.path.basename(self.path)
        self.dirname = os.path.dirname(self.path)
        self.container = container.rstrip('/')

    def __repr__(self):
        return "<InstalledPackage '{}' installed to '{}'>".format(str(self), self.dirname)

    def is_writable(self):
        """Return true if this package is writable (and deletable)"""
        return os.access(self.path, os.W_OK)

    def get_file(self, name):
        """Get filename if it exists"""
        path = os.path.join(self.path, name)
        if os.path.isfile(path):
            with open(path, 'r') as fhl:
                return fhl.read()
        return None

    def get_metadata(self):
        """Returns the metadata from an array of known types"""
        if self._metadata is None:
            for name in ('METADATA', 'PKG-INFO'):
                md_mail = self.get_file(name)
                if md_mail:
                    self._metadata = parse_metadata(md_mail)
        if self._metadata is None:
            md_json = self.get_file('metadata.json')
            if md_json:
                self._metadata = clean_author(json.loads(md_json))
        if self._metadata is None:
            raise KeyError("Can't find package meta data: {}".format(self.path))
        return self._metadata

    def get_description(self):
        """Returns the description from the file or the metadata"""
        desc = self.get_file('DESCRIPTION.rst')
        if not desc:
            return super(InstalledPackage, self).get_description()
        return desc

    def get_files(self, pkg=False):
        """Generates a list of files associated with this package,
        if pkg is true, then it also yields pkg meta data files"""
        record = self.get_file('RECORD')
        if record:
            for line in record.split('\n'):
                if line and ',' in line:
                    (filename, checksum, size) = line.rsplit(',', 2)
                    # XXX Check filesize or checksum?
                    if not filename.startswith(self.base) or pkg:
                        yield os.path.abspath(os.path.join(self.dirname, filename))

    def get_extensions(self):
        """Returns a list of extensions in this package"""
        for fname in self.get_files():
            # XXX We can wrap this filename in an extension manager
            if fname.endswith('.inx'):
                yield fname

    def uninstall(self):
        """Delete the package from the target directory"""
        try:
            for fname in self.get_files(pkg=True):
                self._crop_dirs(fname)
            return True
        except IOError:
            return False

    def _crop_dirs(self, path):
        if self.container:
            if not path.startswith(self.container) or path.rstrip('/') == self.container:
                return # Protected
        try:
            if os.path.isfile(path):
                os.unlink(path)
                print(" [X] {} (file)".format(path))
            elif os.path.isdir(path):
                os.rmdir(path)
                print(" [X] {} (folder)".format(path))
            else:
                print(" [X] {} (gone)".format(path))
                return # Already deleted

            self._crop_dirs(os.path.dirname(path))
        except (IOError, OSError):
            pass

#
# Copyright (C) 2019 Martin Owens
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
"""
Utilities functions for inkscape extension manager.
"""

import os

from collections import defaultdict
from email.parser import FeedParser

DATA_DIR = os.path.join(os.path.dirname(__file__), 'data')
ICON_SEP = ('-' * 12) + 'SVGICON' + ('-' * 12)

def get_description(path):
    """
    Attempts to open two files, one is the long description which
    is populated by reading in a README.md and the second is
    an icon svg which is appeneded to the long description as text.
    """
    if os.path.isfile(path):
        path = os.path.dirname(path)

    if not os.path.isdir(path):
        raise IOError("Path of package must exist, {} not found.".format(path))

    path = os.path.abspath(path)

    readme = os.path.join(path, 'README.md')
    if not os.path.isfile(readme):
        raise ValueError("Can't find readme file '{}'".format(readme))

    icon = os.path.join(path, 'icon.svg')
    if not os.path.isfile(icon):
        raise ValueError("Can't find icon file '{}'".format(icon))

    ret = ''
    with open(readme, 'r') as fhl:
        ret = fhl.read().strip() + '\n\n'

    ret += ICON_SEP + '\n'
    with open(icon, 'r') as fhl:
        ret += fhl.read()
    return ret


def parse_metadata(data):
    """
    Convert older email based meta data into a newer json format,
    See PEP 566 for details.
    """
    feed_parser = FeedParser()
    feed_parser.feed(data)
    metadata = feed_parser.close()

    def getdict():
        """Multi-dimentional dictionary"""
        return defaultdict(getdict)
    ret = defaultdict(getdict)

    ret['description'] = metadata.get_payload()

    if 'Requires-Dist' in metadata:
        ret['run_requires'] = [
            {"requires": metadata['Requires-Dist'].split(',')}
        ]

    for key, value in metadata.items():
        if key == 'Home-page':
            ret['extensions']['python.details']['project_urls']['Home'] = value
        elif key == 'Classifier':
            ret['classifiers'] = list(ret['classifiers'])
            ret['classifiers'].append(value)
        else:
            ret[key.lower().replace('-', '_')] = value

    return ret

def clean_author(data):
    """Clean the author so it has consistant keys"""
    for contact in data.get('extensions', {}).get('python.details', {}).get('contacts', []):
        if contact['role'] == 'author':
            data['author'] = contact['name']
            data['author_email'] = contact.get('email', '')
    return data
